import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {VehiclesListPage} from '../vehicles-list/vehicles-list';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  goToVehicles() {
    this.navCtrl.push(VehiclesListPage);
  }
}
