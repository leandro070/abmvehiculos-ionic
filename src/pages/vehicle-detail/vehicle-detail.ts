import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { Vehicle } from '../../model/vehicle';
import { ModalVehiclePage } from '../modal-vehicle/modal-vehicle';
import { AlertController } from 'ionic-angular';
import { VehicleServiceProvider } from '../../providers/vehicle-service/vehicle-service';



/**
 * Generated class for the VehicleDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-vehicle-detail',
  templateUrl: 'vehicle-detail.html',
})
export class VehicleDetailPage {
  vehicle: Vehicle;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private modalCtrl: ModalController,
    private alertCtrl: AlertController,
    private vehicleService: VehicleServiceProvider
  ) {
    this.vehicle = navParams.get('dataVehicle');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VehicleDetailPage');
  }

  editVehicle(vehicle: Vehicle){
    let modal = this.modalCtrl.create(ModalVehiclePage,{dataVehicle: vehicle, action:'update'});
    modal.present();
  }

  confirmDeleteVehicle(vehicle: Vehicle){
    let alert = this.alertCtrl.create({
      title: 'Delete Vehicle',
      message: 'Do you want to delete this vehicle? This action has no return',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            this.back();
          }
        },
        {
          text: 'Delete',
          handler: () => {
            this.deleteVehicle(vehicle);
          }
        }
      ]
    });
    alert.present();
  }
  deleteVehicle(vehicle: Vehicle){
    this.vehicleService.deleteVehicle(vehicle).subscribe();
    this.back();
  }

  back(){
    this.navCtrl.pop();
  }
}
