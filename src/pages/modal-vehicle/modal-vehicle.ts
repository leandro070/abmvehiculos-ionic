import { Component, Input } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Vehicle } from '../../model/vehicle';
import { VehicleServiceProvider } from '../../providers/vehicle-service/vehicle-service';
import { ToastController } from 'ionic-angular';


/**
 * Generated class for the ModalVehiclePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-modal-vehicle',
  templateUrl: 'modal-vehicle.html',
})
export class ModalVehiclePage {
  vehicle: Vehicle;
  action: string;
  categories = ['motocicleta', 'automovil', 'srv', 'camioneta' , 'camion' , 'micro'];


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private vehiculoService: VehicleServiceProvider,
    private toastCtrl: ToastController,
   ) {

    this.vehicle = navParams.get('dataVehicle');
    this.action = navParams.get('action');
    if (this.action==='create'){
      this.setEmptyVehicle();
    }

  }
  setEmptyVehicle(){
    this.vehicle = {
      brand: '',
      model: '',
      category: '',
      price: '',
      weight: 0,
      year: 0
    }
  }

  ionViewDidLoad() {
  }

  presentToast(message:string): void {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

  saveForm(vehicle:Vehicle){
    if(this.action==='update'){
      this.vehiculoService.updateVehicles(vehicle).subscribe(data => this.presentToast('Vehicle updated'));
    }
    else if (this.action === 'create') {
      this.vehiculoService.createVehicle(vehicle).subscribe(data => this.presentToast('Vehicle created'));
    }
    this.back();
  }

  back(){
    this.navCtrl.pop();
  }
}
