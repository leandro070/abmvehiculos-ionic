import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalVehiclePage } from './modal-vehicle';

@NgModule({
  declarations: [
    ModalVehiclePage,
  ],
  imports: [
    IonicPageModule.forChild(ModalVehiclePage),
  ],
})
export class ModalVehiclePageModule {}
