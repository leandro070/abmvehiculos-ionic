import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading } from 'ionic-angular';
import { Vehicle } from '../../model/vehicle';
import { VehicleServiceProvider } from '../../providers/vehicle-service/vehicle-service';
import { VehicleDetailPage } from '../vehicle-detail/vehicle-detail';
import { ModalVehiclePage } from '../modal-vehicle/modal-vehicle';
import { LoadingController } from 'ionic-angular';


/**
 * Generated class for the VehiclesListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-vehicles-list',
  templateUrl: 'vehicles-list.html',
})
export class VehiclesListPage {
  vehicle: Vehicle;
  vehicles: Vehicle[] = [];
  loader: Loading;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private vehicleService: VehicleServiceProvider,
    private loadingCtrl: LoadingController,
  ) {

  }

  ionViewDidLoad() {
    this.getVehicles();
    this.presentLoading();
  }

  //presento un loader mientras cargo datos
  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Please wait",
      duration: 5000
    });
    this.loader.present();
  }

  getVehicles(): void{
    this.vehicleService.getVehicles().
    subscribe(data => {this.vehicles = data, this.loader.dismiss()}); //una vez recepcionado los datos cancelo el loader
  }

  vehicleSelected(vehicle: Vehicle){
    this.navCtrl.push(VehicleDetailPage, {dataVehicle: vehicle});
  }

  newVehicle(){
    this.navCtrl.push(ModalVehiclePage, {dataVehicle: null, action: 'create'});
  }


}
