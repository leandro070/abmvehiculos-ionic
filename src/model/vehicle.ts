export class Vehicle {
  id?: number;
  model?: string;
  brand?: string;
  passengers_capacity?: string;
  price?: string;
  category?: string;
  year?: number;
  weight?: number;
  created_at?: string;
  updated_at?: string;
}
