import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http'

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { VehiclesListPage } from '../pages/vehicles-list/vehicles-list';
import { VehicleDetailPage } from '../pages/vehicle-detail/vehicle-detail';
import { ModalVehiclePage } from '../pages/modal-vehicle/modal-vehicle';
import { VehicleServiceProvider } from '../providers/vehicle-service/vehicle-service';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    VehiclesListPage,
    ModalVehiclePage,
    VehicleDetailPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    VehiclesListPage,
    ModalVehiclePage,
    VehicleDetailPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    VehicleServiceProvider,
  ]
})
export class AppModule {}
