import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of } from 'rxjs/observable/of';
import { catchError, tap } from 'rxjs/operators';
import { Vehicle } from '../../model/vehicle';
import { Observable } from 'rxjs/Observable';


/*
  Generated class for the VehicleServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class VehicleServiceProvider {
  private vehiculosUrl = 'https://shielded-beyond-99836.herokuapp.com/vehicles';

  constructor(public http: HttpClient) {
    console.log('Hello VehicleServiceProvider Provider');
  }

  getVehicles(): Observable<Vehicle[]> {
    return this.http.get<Vehicle[]>(this.vehiculosUrl, httpOptions)
    .pipe(
      tap(vehicles => console.log('Successful search of vehicles')),
      catchError(this.handleError(`getVehicles`,[]))
    );
  }

  updateVehicles(vehicle: Vehicle): Observable<any> {
    const url = this.vehiculosUrl + '/' + vehicle.id;
    return this.http.put(url, vehicle, httpOptions)
    .pipe(
      tap(data => console.log(`Update vehicle ${data.id}`),
      catchError(this.handleError(`updateVehicles`, []))
      )
    );
  }

  deleteVehicle(vehicle: Vehicle): Observable<any>{
    const url = this.vehiculosUrl + '/' + vehicle.id;
    return this.http.delete(url, httpOptions).pipe(
      tap(data => console.log('Delete vehicle')),
      catchError(this.handleError(`Error: deleteVehicles`,[]))
    );
  }

  createVehicle(vehicle: Vehicle): Observable<any>{
    console.log(vehicle.brand + " " + vehicle.model + " " + vehicle.category + " " + vehicle.price + " " + vehicle.weight + " " + vehicle.year);
    return this.http.post<Vehicle>(this.vehiculosUrl, vehicle, httpOptions).pipe(
      tap( data=> console.log('Create vehicle')),
      catchError(this.handleError(`Error: createVehicles`, []))
    );
  }


  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
